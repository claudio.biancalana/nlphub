import flask
from flask import request
import spacy
import json

app = flask.Flask(__name__)

class EntityGenerator(object):
    
    _slots__ = ['text']
    
    def __init__(self, text=None):
        self.text = text
        
    def get(self):
        """
        Return a Json
        """
        nlp = spacy.load("it_core_news_lg")
        doc = nlp(self.text)
        text = [ent.text for ent in doc.ents]
        entity = [ent.label_ for ent in doc.ents]
    
        from collections import Counter
        import json

        data = Counter(zip(entity))
        unique_entity = list(data.keys())
        unique_entity = [x[0] for x in unique_entity]

        d = {}
        for val in unique_entity:
            d[val] = []

        for key,val in dict(zip(text, entity)).items():
            if val in unique_entity:
                d[val].append(key)
        return d

@app.route('/', methods=['GET'])
def home():
    testo = request.args.get('text')
    helper = EntityGenerator(text=testo)
    response = helper.get()
    return (json.dumps(response , indent=3))

app.run(host='0.0.0.0')
