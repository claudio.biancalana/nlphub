FROM python:3.9.1

WORKDIR /usr/src/app

RUN pip install flask
RUN pip install -U pip setuptools wheel
RUN pip install -U spacy
RUN python -m spacy download it_core_news_lg

COPY . /usr/src/app

# For FLASK
EXPOSE 5000
CMD ["python", "nlp.py"]
